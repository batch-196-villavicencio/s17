/*
	1. Create a function which is able to prompt the user to provide their fullname, age, and location.
		-use prompt() and store the returned value into a function scoped variable within the function.
		-display the user's inputs in messages in the console.
		-invoke the function to display your information in the console.
		-follow the naming conventions for functions.
*/
	//first function here:
	function myInfo(){
		function pInfo(){
			fullName = prompt('Enter your full name:');
			console.log("Hi " + fullName);
			age = prompt('Enter your age:');
			console.log(fullName + "\'s age is " + age);
			loc = prompt('Enter your location:');
			console.log(fullName + " is located at " + loc);
		};
		pInfo();
	};
	myInfo();

/*
	2. Create a function which is able to print/display your top 5 favorite bands/musical artists.
		-invoke the function to display your information in the console.
		-follow the naming conventions for functions.
	
*/
	//second function here:
	function fBand(){
		let bands = ['Fall Out Boy','Panic at the Disco','PARD','My Chemical Romance','Eminem'];
		console.log(bands);
	};
	fBand();

/*
	3. Create a function which is able to print/display your top 5 favorite movies of all time and show Rotten Tomatoes rating.
		-Look up the Rotten Tomatoes rating of your favorite movies and display it along with the title of your favorite movie.
		-invoke the function to display your information in the console.
		-follow the naming conventions for functions.
	
*/
	//third function here:
	function fMovies(){
		let mov1 = "Shawshank Redemption";
		let mov2 = "Forrest Gump";
		let mov3 = "Parasite";
		let mov4 = "Prestige";
		let mov5 = "Spider-Man: Into the Spider-Verse";
		let tomato = "Tomatometer for ";
		console.log("1. The " + mov1);
		console.log(tomato + mov1 + ": 91%");
		console.log("2. " + mov2);
		console.log(tomato + mov2 + ": 71%");
		console.log("3. " + mov3);
		console.log(tomato + mov3 + ": 99%");
		console.log("4. The " + mov4);
		console.log(tomato + mov4 + ": 76%");
		console.log("5. " + mov5);
		console.log(tomato + mov5 + ": 97%");
	}
	fMovies();

/*
	4. Debugging Practice - Debug the following codes and functions to avoid errors.
		-check the variable names
		-check the variable scope
		-check function invocation/declaration
		-comment out unusable codes.
*/

// printUsers();
let printFriends = function printUsers(){
	alert("Hi! Please add the names of your friends.");
	let friend1 = prompt("Enter your first friend's name:"); 
	let friend2 = prompt("Enter your second friend's name:"); 
	let friend3 = prompt("Enter your third friend's name:");

	console.log("You are friends with:")
	console.log(friend1); 
	console.log(friend2); 
	console.log(friend3); 
};
printFriends();


// console.log(friend1);
// console.log(friend2);